import { useEffect, useState } from 'react';
import './App.css';
import { HangImage } from './components/HangImage';
import { getRamdomWord } from './helpers/getRamdomWord';
import {letters} from './helpers/letters';

function App() {

  const [ word, setWord] = useState(getRamdomWord);
  const [ hiddenWord, setHiddenWord ] = useState('_ '.repeat(word.length));
  const [ attemps, setAttempts ] = useState(0);
  const [ lose, setLose ] = useState(false);
  const [ won, setWon ] = useState(false);

  useEffect(() => {
    if(attemps >= 9){
      setLose(true);
    }
  }, [attemps]);

  useEffect(() => {
    const currentHiddenWord = hiddenWord.split(' ').join('');
    if( currentHiddenWord === word){
      setWon(true);
    }
  }, [hiddenWord]);

  const checkLetter = (letter: string) => {
    if(lose) return;

    if(won) return;
    
    if(!word.includes(letter)) {
      setAttempts(Math.min(attemps + 1, 9));
      return;
    };

    const hiddenWordArray = hiddenWord.split(' ');

    for(let i = 0; i<word.length; i++) {
      if(word[i] == letter) {
        hiddenWordArray[i] = letter;
      }
      setHiddenWord(hiddenWordArray.join(' '));
    }

  };

  const newGame = () => {
    const newWord = getRamdomWord();
    setWord(newWord);
    setHiddenWord('_ '.repeat(newWord.length));
    setAttempts(0);
    setLose(false);
    setWon(false);
  }

  return (
    <div className="App">
      {/*imagenes*/}
      <HangImage imageNumber={attemps}/>
      {/*palabra oculta*/}
      <h3>{hiddenWord}</h3>
      {/*contador de intentos*/}
      <h3>Intentos: {attemps}</h3>
      {/*botones de letras*/}
      {
        (lose)
          ? <h2>Perdio {word}</h2>
          : ''
      }
       {
        (won)
          ? <h2>Felicidades usted gano!</h2>
          : ''
      }
      {
        letters.map((letter) => ( 
          <button 
            onClick= { () => checkLetter(letter)}
            key={ letter }> 
            { letter }
          </button>
        ))
        }
        <br></br>
        <button onClick = { newGame }>
          Nuevo Juego
        </button>
    </div>
  );
};

export default App;
//npm run dev correr el proyecto

